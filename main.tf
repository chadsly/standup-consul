# Log into AWS
provider "aws" {
  #version = "~> 2.0"
  region = var.aws_region
  access_key = var.AWS_ACCESS_KEY_DIB
  secret_key = var.AWS_SECRET_KEY_DIB
}


resource "aws_instance" "consul" {
  ami           = var.aws_ami
  instance_type = "t2.micro"
  count= var.consul_count
  subnet_id = "${aws_subnet.main.id}"
  security_groups = ["${aws_security_group.allow_stuff.id}"]
  key_name = "consul"
  lifecycle {
    create_before_destroy = true
  }
  tags = {
    Name  = "Consul-${count.index + 1}"
    1067  = "N/A"
    Application = "Consul"
    "Application Role" = "Service Management"
    "Cost Center" = "AFLCMC, AFRL"
    Email = "chad@expansiagroup.com"
    Environment = "DEV"
    Group = "HBG, RIEBB"
    OS = "RHEL_7"
    Organization = "AF DCGS, AFRL"
    Owner = "Chad Sylvester"
    Project = "CI/CD"
  }
}