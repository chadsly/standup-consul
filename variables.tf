variable "aws_region"{
  default = "us-east-1"
}

variable "AWS_ACCESS_KEY"{
  default = ""
}

variable "AWS_SECRET_KEY"{
  default = ""
}

variable "aws_ami"{
  default=""
}

variable "vpcid" {
  default="1"
}

variable "consul_address" {
  type    = list(string)
  default = ["1.1.1.1"]
}

variable "consul_count" {
  default= 3
}