---
# vars file for consul

true_or_false: "true"
number_of_servers: ${number_of_servers}
enable_ui: "true"
bind_addr: '"bind_addr"'
array_ips: '[ %{ for addr in ip_addrs ~}
"${addr}" ,
%{ endfor ~}]'
logging_verbosity: INFO

name_data_center: cie 
opt_consul: "/opt/consul"
# encrypt: "{{ encryption_key }}"
encrypt: "3lg9DxVfKNzI8O+IQ5Ek+Q=="

cleanup_dead_servers:  "true,"
last_contact_threshold:  '"200ms",'
max_trailing_logs: "250,"
server_stabilization_time: '"10s",'
redundancy_zone_tag: '"zone",'
disable_upgrade_migration: "false,"
upgrade_version_tag: '"",'