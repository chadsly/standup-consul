terraform {
#  backend "local" {
#    path = "terraform.tfstate"
#  }
  backend "s3" {
    bucket = "consul-standup-tf-s3"
    key    = "aws/terraform.tfstate"
    region = "us-east-1"
  }
}