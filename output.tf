output "region" {
  value = var.aws_region
}
output "consul_ip" {
  value = "${aws_instance.consul.*.public_ip}"
}

output "consul_hostnames" {
  value = "${aws_instance.consul.*.public_dns}"
}

output "consul_count" {
  #value = "${length({aws_instance.consul.*.public_dns)}"
  value = "${length(aws_instance.consul)}"
}

output "rendered" {
  value = templatefile("hosts_template.tpl", {ip_addrs = aws_instance.consul.*.public_dns})
}

output "rendered_vars" {
  value = templatefile("vars.tpl", {ip_addrs = aws_instance.consul.*.public_ip, number_of_servers=length(aws_instance.consul)})
}