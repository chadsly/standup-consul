data "aws_vpc" "main" {
  id = var.vpcid
}

data "aws_internet_gateway" "main" {
  filter {
    name   = "attachment.vpc-id"
    values = ["${data.aws_vpc.main.id}"]
  }
}

resource "aws_subnet" "main" {
  vpc_id     = "${data.aws_vpc.main.id}"
  cidr_block = "172.31.160.0/24"
  map_public_ip_on_launch = true
  tags = {
    Name = "Main"
  }
}

resource "aws_route_table" "r" {
  vpc_id = "${data.aws_vpc.main.id}"
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${data.aws_internet_gateway.main.id}"
  }

  tags = {
    Name = "main"
  }
}

resource "aws_route_table_association" "rta_subnet_public" {
  subnet_id      = "${aws_subnet.main.id}"
  route_table_id = "${aws_route_table.r.id}"
}

resource "aws_security_group" "allow_stuff" {
  name        = "allow_stuff"
  description = "Allow TLS inbound traffic"
  vpc_id     = "${data.aws_vpc.main.id}"
    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"] 
    }   
    ingress {
        from_port = 3000 
        to_port = 3000
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"] 
    }   
    ingress {
        from_port = 443
        to_port = 443 
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port = 8200 
        to_port = 8200
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
   }
   
    ingress {
        from_port = 8201 
        to_port = 8201
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
   }
    ingress {
        from_port = 8300 
        to_port = 8300
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
   }
    ingress {
        from_port = 8301 
        to_port = 8301
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
   }
    ingress {
        from_port = 8500 
        to_port = 8500
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
   }
    ingress {
        from_port = 8600 
        to_port = 8600
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
   }
   ingress {
        from_port = -1
        to_port = -1
        protocol = "icmp"
        cidr_blocks = ["0.0.0.0/0"]
   }
   egress {
        from_port = 0 
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
   }
  tags = {
    Name = "allow_stuff"
  }
}